# micromanager slide explorer exporter 

Beanshell script to export tiles of Slide Explorer plugin of micromanager. 
The Slide Explorer plugin store in a folder tiles (cropped of 8 pixels on each sides) as 8-bit tif with a resolution pyramid like behavior. 
This script find the way to reload the correct tiles from file and extract their positions (on the window canvas and on the microscope stage) and save tiles as separate tif files and coordinates in a text file compatible with [Grid/Collection Stitching plugin](https://imagej.net/Image_Stitching#Grid.2FCollection_Stitching). This plugin could then be used to fuse the mosaic from the *TileConfiguration.txt* file created by the script.

Their is two versions:
- **Micro-manager 1.4** branch: **save_slideexplorer_MM1_4.bsh**
- **Micro-manager 2.0** branch: **save_slideexplorer_MM2.bsh**

# Install
1. Download the script: 
2. Open micro manager
3. Open the script editor: **Tools > Script Pannel**
4. Use the **Add** button to add the *save_slideexplorer.bsh* file to script-shortcuts.

# How to use the script
1. Run the slide explorer plugin (and explore your sample)
2. Set the slide explorer to **Navigate** mode (keep the slide explorer open !)
3. Open the script editor:  **Tools > Script Pannel**
4. Clic on the **save_slideexplorer.bash** in *script-shortcuts* list (if it's not present check Install instructions)
5. Clic on the **run** button and give a destination folder 

The script will output each tiles as:
- SlideExplorer-tile-1.tif
- SlideExplorer-tile-2.tif
- ...
- SlideExplorer-tile-n.tif

And two text files formated to be compatible with Grid/Collection stitching FiJi plugin):
- TileConfiguration.txt with canvas coordinates (x,y)
```text
# Tiles from Slide Explorer zoom level 0
dim = 2
# Tile canvas coordinates (in pixel)
SlideExplorer-tile-1.tif; ; (-2016,1008)
SlideExplorer-tile-2.tif; ; (-3528,3528)
```
- TileStageCoordinates.txt with Stage coordinates (x,y)
```text
# Tiles from Slide Explorer zoom level 0
dim = 2
# Tile stage coordinates
SlideExplorer-tile-1.tif; ; (-2016.0,1008.0)
SlideExplorer-tile-2.tif; ; (-3528.0,3528.0)
```

# How to stitch your tiles using Fiji
1. Open Fiji
2. Start [Grid/Collection Stitching plugin](https://imagej.net/Image_Stitching#Grid.2FCollection_Stitching): **Plugins > Stitching > Grid/Collection stitching**
3. Select **Type: Position from file** and **Order: Defined by TileConfiguration** and press **Ok**
4. Set the directory to the destination folder given to save tiles of slide explorer. Set **layout file** to **TileConfiguration.txt**, untick all cases and press **ok**
5. You can save tour tile using imageJ menu: **File > Save** or **File > Save as > ...**
